export function fight(firstFighter, secondFighter) {
  let firstHP = firstFighter.health,
      secondHP = secondFighter.health;
  let turn = 1;
  while (firstHP > 0 && secondHP > 0) {
    console.log(firstHP, secondHP);
    if (turn == 1) {
      secondHP -= getDamage(firstFighter, secondFighter);
      turn = 2;
    } else {
      firstHP -= getDamage(secondFighter, firstFighter);
      turn = 1;
    }
  }
  if (firstHP <= 0) {
    return secondFighter;
  } else {
    return firstFighter;
  }
}

export function getDamage(attacker, enemy) {
  let damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  return fighter.attack * (Math.random() + 1);
}

export function getBlockPower(fighter) {
  return fighter.defense * (Math.random() + 1);
}
