import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source } = fighter;

  console.log(name, attack, defense, health, source);

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  const nameElement = createElement({ tagName: 'span', className: 'fighter' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter' });

  // show fighter name, attack, defense, health, image

  nameElement.innerText = `Name: ${name}`;
  attackElement.innerText = `Attack: ${attack}`;
  defenseElement.innerText = `Defense: ${defense}`;
  healthElement.innerText = `Health: ${health}`;
  fighterDetails.append(imgElement);
  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(healthElement);

  return fighterDetails;
}
