import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  const title = 'Winner';

  const { name, source } = fighter;

  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const attributes = { src: source };
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes });
  const nameElement = createElement({ tagName: 'span', className: 'fighter' });

  nameElement.innerText = `Name: ${name}`;
  bodyElement.append(imgElement);
  bodyElement.append(nameElement);

  showModal({ title, bodyElement });
  
}
